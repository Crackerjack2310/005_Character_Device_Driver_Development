#include"header.h"
#include"structures.h"
#include"declarations.h"

Qset* create_dev_buff(size_t size)
{
	Qset *lqset, *first;
	int lsize, noi, noq, i;

	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	
	lsize = size;
	if (size > dev_size)
		lsize = dev_size;
	
	#ifdef DEBUG
	printk(KERN_INFO "lsize : %d\n", lsize);
	#endif
	
	noi = lsize / (reg_size*no_of_registers);
	if (lsize % (reg_size*no_of_registers))
		noi++;
						/* noi will decide the no of items or Qsets required */

////////////////////////////////////////////////////////// now allocating for items

	#ifdef DEBUG
	printk(KERN_INFO "noi : %d\n", noi);
	#endif
	
	lqset = first = NULL;
	for(i = 0; i < noi; i++)
	{
//		printk(KERN_INFO "Creating Qset : %d\n", i);
		if (i == 0)		/*for first node*/
		{
			first = lqset = (Qset*)kmalloc(sizeof(Qset), GFP_KERNEL);
			if (!first)
			{
				#ifdef DEBUG
				printk(KERN_ERR "Kmalloc failed for first\n");
				#endif
				goto OUT;
			}
			memset(first, '\0', sizeof(Qset));	
		}
		else
		{
			lqset->next = (Qset*)kmalloc(sizeof(Qset), GFP_KERNEL);
			if (!lqset->next)
			{
				#ifdef DEBUG
				printk(KERN_ERR "Kmalloc failed for lqset\n");
				#endif
				goto OUT;
			}
			memset(lqset->next, '\0', sizeof(Qset));	
			lqset = lqset->next;
		}
//		printk(KERN_INFO "Created items : %d\n", i);
	}

////////////////////////////////////////////////////////// now allocating for array in each Qset 

	lqset = first;				// getting back to start node
	if (!lqset)
	{
		#ifdef DEBUG
		printk(KERN_ERR "lqset is NULL\n");
		#endif
		goto OUT;
	}
	printk(KERN_INFO "beginning 2nd loop for allocating %d arrays\n", noi);

	for(i = 0; i < noi; i++)
	{
//		printk(KERN_INFO "loop %d\n", i);
		lqset->data = kmalloc(sizeof(char*)*no_of_registers, GFP_KERNEL);
//		lqset->data = (void **) kmalloc(sizeof(char*)*no_of_registers, GFP_KERNEL);
//		printk(KERN_INFO "kmalloc done for %d\n", i);
		if (!lqset->data)
		{
			#ifdef DEBUG
			printk(KERN_ERR "Kmalloc failed for lqset->data\n");
			#endif
			goto OUT;
		}
		memset(lqset->data, '\0', sizeof(char*)*no_of_registers);
		lqset = lqset->next;			// update list
	}
	
////////////////////////////////////////////////////////////////////// now allocating for each quantum

	noq = lsize / reg_size;
	if (lsize % reg_size)
		noq++;

	#ifdef DEBUG
	printk(KERN_INFO "noq : %d\n", noq);
	#endif
	
	lqset = first;
	if (!lqset)
	{
		#ifdef DEBUG
		printk(KERN_ERR "lqset is NULL\n");
		#endif
		goto OUT;
	}
	for(i = 0; i < noq; i++)
	{
//		printk(KERN_ERR "loop %d\n", i);
		
		lqset->data[i] = kmalloc(sizeof(char)*reg_size, GFP_KERNEL);
		if (!lqset->data[i])
		{
			#ifdef DEBUG
			printk(KERN_ERR "Kmalloc failed for lqset->data[i]\n");
			#endif
			goto OUT;
		}
		memset(lqset->data[i], '\0', sizeof(char)*reg_size);
		
		if (i == (no_of_registers - 1))
		{
			noq = noq - no_of_registers;		
			i = -1;
			lqset = lqset->next;			// update list
		}
	}
		
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return first;
OUT:	
	#ifdef DEBUG
	printk(KERN_ERR "Error\n");
	#endif
	return NULL;
}
