#include"header.h"
#include"structures.h"
#include"declarations.h"

ssize_t writedev(struct file *file_ptr, const char __user *ubuff, size_t size, loff_t *offset)
{
	Dev *ldev;
	Qset *dqset;
	int nbtw, flag, lsize, swc, i, ret = 0;
	swc = i = 0;
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	
	ldev = file_ptr->private_data;
	lsize = size;
	if (size > dev_size)
	{
		#ifdef DEBUG
		printk("Data will be written partially\n");
		#endif
		lsize = dev_size;
	}
	dqset = create_dev_buff(size);	
	if (!dqset)
	{
		#ifdef DEBUG
		printk(KERN_INFO "\n");
		#endif
		goto OUT;
	}
	ldev->first = dqset;
	flag = 1;
	nbtw = reg_size;
	while(flag)
	{
//		printk(KERN_INFO "loop %d\n", flag);
		if (lsize <= reg_size)
		{
			nbtw = lsize;		// no of bytes to write if less than reg_size
			flag = 0;
		}
		ret = __copy_from_user(dqset->data[i], ubuff, nbtw);	// shifting pointer to the next value
		ubuff = ubuff + nbtw;
		#ifdef DEBUG
//		printk(KERN_INFO "data : %s\n", (char *)dqset->data[i]);
		#endif
		if(ret)						// > 0 for the no of bytes not written
		{
			#ifdef DEBUG
			printk(KERN_INFO "data bytes lost : %d\n", ret);
			#endif
		}	
		
		swc += reg_size - ret;				// successfully written characters	
		lsize -= reg_size - ret;	
		
		if (i == (no_of_registers - 1))
		{
			i = -1;
			
			dqset = dqset->next;	// ????	
		}
		i++;
	}
	swc = swc - (reg_size - nbtw);
	ldev->data_size =  swc;				// update datasize for this device
	
	
	#ifdef DEBUG
	printk(KERN_INFO "data_size : %d\n", swc);
	#endif

	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return swc; 
OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "Error\n");
	#endif
	return -1;
}
