struct file_operations fops =
{
	open 	: opendev,
	release : releasedev,
	write 	: writedev,
//	read    : readdev_dest,
	read    : readdev,
	llseek  : llseekdev
};


