#include"header.h"
#include"declarations.h"

unsigned int majorno, minorno, nod, ret;
dev_t dev_id;

static int __init init_func(void)
{
	printk(KERN_INFO "Hello Kernel !!!\n");

	majorno = MAJORNO;
	minorno = MINORNO;
	nod = NOD;
												
	if (majorno)		// device major no not 0, so enter to register
	{
		ret = register_chrdev_region(dev_id, nod, DEVNAME);
		if (ret == -1)
			goto OUT;
		printk(KERN_INFO "We entered in register_chrdev_region with Major no : %d\n", majorno);
	}	
	else
	{	ret = alloc_chrdev_region(&dev_id, minorno, nod, DEVNAME);
		if (ret == -1)
			goto OUT;
	
		printk(KERN_INFO "Major no : %d\n", MAJOR(dev_id));
		printk(KERN_INFO "Minor no : %d\n", MINOR(dev_id));
		printk(KERN_INFO "Device_ID : %d\n", dev_id);
		printk(KERN_INFO "MKDEV() value : %d\n", MKDEV(majorno, minorno));
	}	
	my_module();			/* calling the module from kernel space */
		return 0;
OUT:	
	printk(KERN_ERR "alloc_chrdev_region failed with return value : %d !!\n", ret);
	return 1;
}
module_init(init_func);
