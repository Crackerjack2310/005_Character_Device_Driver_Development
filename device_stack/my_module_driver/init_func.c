#include"header.h"
#include"declarations.h"

EXPORT_SYMBOL(my_module);
static int __init init_func(void)
{
	printk(KERN_INFO "my_module says : Hello Kernel !!!\n");
	return 0;
}
module_init(init_func);
