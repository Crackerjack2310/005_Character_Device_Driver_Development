#include"header.h"
#include"structures.h"
#include"declarations.h"

ssize_t readdev(struct file *file_ptr, char __user *ubuff, size_t size, loff_t *offset)
{
	Dev *ldev;
	Qset *dqset, *snext;
	int nbtr, lsize, src = 0, i = 0, ret = 0, k, count = 0;
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "offset : %ld \n", (long)*offset);
	#endif
	
/*	#ifdef DEBUG
	printk(KERN_INFO "%s : f_pos value : %ld\n", __func__, (long)file_ptr->f_pos);
	#endif
*/
	ldev = file_ptr->private_data;		// retrieving info from struct file
	lsize = size;
	if (size > dev_size)
	{
		#ifdef DEBUG
		printk(KERN_INFO "Specified datasize is larger than device datasize\n");
		#endif
		lsize = dev_size;
	}						// lsize is the amount of data to read		
	snext = dqset = ldev->first;				
	nbtr = reg_size;
	while(1)
	{
		if (!dqset->data[i])
			break;
		nbtr = k = 0;	
		// printk(KERN_INFO "%s : loop %d 	   src %d\n", __func__, i, src);
		while (k < 4)			// check for each character in a quantum to be '\0'
		{
			if (*(char*)(dqset->data[i] + k) == '\0')	// if value of kth byte in ith row is '\0'
				break;
			nbtr = k;
			k++;
		}
		nbtr++;				// as nbtr is always 1 less when in loop, hence +1
		nbtr = ((src + nbtr) < lsize) ? nbtr : (nbtr - ((src + nbtr) - lsize));
		
		if (src >= (*offset))	// ?? need to change for first regsize issue..multiple of 
		{
			ret = __copy_to_user(ubuff, dqset->data[i], nbtr);	// shifting pointer to the next value
			ubuff = ubuff + reg_size;
			if(ret)						// > 0 for the no of bytes not written
			{
				#ifdef DEBUG
				printk(KERN_INFO "data bytes lost : %d\n", ret);
				#endif
			}		
			count += nbtr - ret; 
		}
		
		src += (nbtr - ret);				// successfully read characters	
		if (i == (no_of_registers - 1))
		{
			if (!dqset->next)
				break;
			dqset = dqset->next;	
			i = -1;
		}
		i++;
	}
	ldev->data_size = count;				// update datasize for this file structure
	*offset += count;	
/*	file_ptr->f_pos += count;
	#ifdef DEBUG
	printk(KERN_INFO "f_pos updated to : %ld \n", (long)file_ptr->f_pos);
	#endif
*/	
	#ifdef DEBUG
	printk(KERN_INFO "offset updated to : %ld \n", (long)*offset);
	#endif

	#ifdef DEBUG
	printk(KERN_INFO "data read : %d bytes\n", count);
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return count; 					// no of bytes successfully read
/*OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "Error\n");
	#endif
	return -1;*/
}
