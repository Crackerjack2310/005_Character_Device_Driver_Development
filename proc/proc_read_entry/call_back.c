#include"header.h"
#include"structures.h"
#include"declarations.h"

int call_back(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	int len = 0;	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	len += sprintf(page + len, "register size : %d\n", reg_size); 	
	len += sprintf(page + len, "no of reg     : %d\n", no_of_registers); 	
	len += sprintf(page + len, "datasize      : %d\n", data_size); 	
	len += sprintf(page + len, "device size   : %d\n", dev_size); 	
	*eof = 1;
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return len;
}
