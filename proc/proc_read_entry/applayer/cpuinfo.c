#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>

int main()
{
	int arg = 2, fd, count;
	char buffer[BUFSIZ], *ptr;
	fd = open("/proc/cpuinfo", O_RDONLY);
	if (fd == -1)
	{
		perror("open :");
		exit(0);
	}
	count = read(fd, buffer, sizeof(buffer));
	buffer[count] = '\0';
	//printf("%s\n", buffer);
	ptr = strstr(buffer, "cpu MHz");
	if (!ptr)
	{
		perror("strstr");
		exit(0);
	}
	sscanf(ptr, "cpu MHz : %d\n", &arg);
	printf("CPU speed : %d\n", arg);
	return 0;
}
