#include"header.h"
#include"declarations.h"

int llseek_into_device(int fd)
{
	printf("\n%s : Begin\n", __func__);
	int ret, offset, origin;
	
	printf("\n%s : Enter origin as \n", __func__);
	printf("________Begining :  0________\n");
 	printf("________Current  :  1________\n");
 	printf("________End  	 :  2________\n");
	
	printf("\n%s : Enter origin : \n", __func__);
	scanf("%d", &origin);
	
	printf("\n%s : Enter offset : \n", __func__);
	scanf("%d", &offset);

	ret = lseek(fd, offset, origin);
	if (ret == -1)
	{
		perror("lseek error");
		exit(EXIT_FAILURE);
	}
	printf("\n%s : new position : %d\n", __func__, ret);
	
	printf("\n%s : End\n", __func__);
	return 0;
}
