int opendev(struct inode *, struct file *);
int releasedev(struct inode *, struct file *);
loff_t llseekdev(struct file *, loff_t, int);
ssize_t readdev(struct file *, char __user *, size_t, loff_t *);
ssize_t readdev_dest(struct file *, char __user *, size_t, loff_t *);
ssize_t writedev(struct file *, const char __user *, size_t, loff_t *);
int trim_device(Dev *);
Qset* create_dev_buff(Dev*, size_t);
extern Dev *device;
extern unsigned int majorno, minorno, nod, dev_id, no_of_registers, reg_size, dev_size, data_size;
int call_back(char *page, char **start, off_t off, int count, int *eof, void *data);
#ifndef DEVNAME
#define DEVNAME "Sys_call_hijaker_device"
#endif

#ifndef MAJORNO 
#define MAJORNO 0
#endif

#ifndef MINORNO 
#define MINORNO 0
#endif

#ifndef NOD 
#define NOD 5
#endif

#ifndef DEBUG	
#define DEBUG
#endif

#ifndef NO_OF_REG	
#define NO_OF_REG 8
#endif

#ifndef REG_SIZE	
#define REG_SIZE 4
#endif

#ifndef DEV_SIZE	
#define DEV_SIZE 1024
#endif

#ifndef DATA_SIZE	
#define DATA_SIZE 0
#endif
