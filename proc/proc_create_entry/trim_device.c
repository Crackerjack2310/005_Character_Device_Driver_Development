#include"header.h"
#include"structures.h"
#include"declarations.h"

int trim_device(Dev *ldev)
{
	Qset *lqset, *slast;
	int i, k = 0;
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	if (!ldev)				// No device exists
	{
		#ifdef DEBUG
		printk(KERN_ERR "No Device Found !!!\n");
		#endif
		goto OUT;
	}
	if (!ldev->first)			// device exists but empty
	{
		#ifdef DEBUG
		printk(KERN_ERR "Nothing to Trim !!\n");
		#endif
		goto OUT;
	}

/////////////////////////////////////////////////////////////// if atleast one item exists	
	
	while (ldev->first)
	{
		#ifdef DEBUG
		printk(KERN_INFO "loop %d\n", k);
		#endif
		slast = lqset = ldev->first;
		while (lqset->next)		// if more than one node...get to last.
		{
			slast = lqset;		// slast holds the address of current item you free at last
			lqset = lqset->next;	// update lqset
		}	
	
		for (i = (ldev->no_of_registers - 1); i >= 0; i--)
		{
			if (lqset->data[i])
			{
				kfree(lqset->data[i]);		// freeing each quantum
				lqset->data[i] = NULL;		// dangling * case removal
			}
		}		
		
		kfree(lqset->data);		// freeing each array in an item
		lqset->data = NULL;		// dangling * case removal
		
		if (slast->next)
		{
			kfree(slast->next);		// freeing each item using its address from slast
			slast->next = NULL;		// dangling * case removal
		}
		else
		{
			kfree(slast);			// freeing each item using its address from slast
			ldev->first = lqset = slast = NULL;		// dangling * case removal	
		}
		k++;
	}	
	
OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return 0;
}

