#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>

int main(int argc, char*argv[])
{
	int arg = 0, fd, count;
	char buffer[BUFSIZ], path[100];
	snprintf(path, sizeof(path), "/proc/%s/cmdline", argv[1]);
	printf("%s\n", path);
	fd = open(path, O_RDONLY);
	if (fd == -1)
	{
		perror("open");
		exit(0);
	}
	count = read(fd, buffer, sizeof(buffer));
	buffer[count] = '\0';
	printf("%s\n", buffer);
	return 0;
}
