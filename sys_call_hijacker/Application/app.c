#include"header.h"
#include"declarations.h"

int main()
{
	printf("\n%s : Begin \n", __func__);

	int choice, ch, rfd, wfd;
	rfd = wfd = -1;	
	
	while(1)
	{
		choice = mainmenu();	
		switch (choice)
		{
			case 1 :
				printf("Enter 0 {O_RDONLY} OR 1 {O_WRONLY}\n");
				scanf("%d", &ch);
					
				if (ch == 0)
				{
					rfd = open("My_Dev", O_RDONLY);
					if (rfd == -1)
					{
						perror("open() failed : ");
						break;
					}						
					printf("File Opened in readonly mode with fd = %d\n", rfd);
					wfd = -1;
				}
				else if (ch == 1)
				{
					wfd = open("My_Dev", O_WRONLY);
					if (wfd == -1)
					{
						perror("open() failed : ");
						break;
					}						
					printf("File Opened in writeonly mode with fd = %d\n", wfd);
					rfd = -1;
				}
				else
					printf("Invalid Choice\n");
				break;
			case 2 :
				if (wfd == -1)
				{
					printf("%s : file not opened for writing yet\n", __FILE__);
					break;
				}
				write_to_device(wfd);
				break;
		
			case 3 :
				if (rfd == -1)
				{
					printf("%s : file not opened for reading yet\n", __FILE__);
					break;
				}
				read_from_device(rfd);
				break;
			
			case 4 :
				if (rfd == -1)
				{
					printf("%s : file not opened for seeking yet\n", __FILE__);
					break;
				}
				llseek_into_device(rfd);
				break;
			
			case 5 :
				if (wfd == -1)
				{
					printf("%s : cannot close, file not opened yet\n", __FILE__);
					break;
				}
				close(wfd);
				printf("File closed for fd = %d\n", wfd);
				break;
			
			default:
				printf("\nOHK !! Exiting\n\v");
				goto END;
		}	
	}

END:	printf("\n%s : END\n\n", __func__);
	return 0;
}
