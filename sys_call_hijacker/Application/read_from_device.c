#include"header.h"
#include"declarations.h"

int read_from_device(int fd)
{
	printf("\n%s : Begin\n", __func__);
	int count;
	char buffer[BUFSIZ];
	memset(buffer, '\0', BUFSIZ);
	count = read(fd, buffer, 45);
	printf("\n%s : read %d bytes from file\n", __func__, count);
	printf("\n%s : %s\n", __func__, buffer);
	printf("\n%s : END\n", __func__);

	return 0;
}
