#include"header.h"
#include"structures.h"
#include"declarations.h"

ssize_t readdev_dest(struct file *file_ptr, char __user *ubuff, size_t size, loff_t *offset)
{
	Dev *ldev;
	Qset *dqset, *snext;
	int nbtr, lsize, src = 0, i, ret = 0, k;
	
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	ldev = file_ptr->private_data;		// retrieving info from struct file
	if (!ldev)				// if invalid ...return	
	{
		printk(KERN_INFO "Nothing to read !!!\n");
		goto OUT;
	}
	i = ldev->rd_flag;	// 0 if fresh read...else row no. to read from following previous partial readdev_dest
	lsize = size;					
	if (size > ldev->data_size)
	{
		#ifdef DEBUG
		printk(KERN_INFO "user asked to read more than whats written !!\n");
		#endif
		lsize = ldev->data_size;
	}						// lsize is the amount of data to read		
	snext = dqset = ldev->first;				
	if (!dqset)				
	{
		printk(KERN_INFO "Nothing to read !!!\n");
		goto OUT;
	}
	nbtr = ldev->reg_size;				
	while(src < lsize)
	{
		nbtr = k = 0;	
		while (k < ldev->reg_size)			// check for each character in a quantum to be '\0'
		{
			if (*(char*)(dqset->data[i] + k) == '\0')	// if value of kth byte in ith row is '\0'
				break;
			nbtr = k;
			k++;
		}
		nbtr++;				// as nbtr is always 1 less when in loop, hence +1
		nbtr = ((src + nbtr) < lsize) ? nbtr : (nbtr - ((src + nbtr) - lsize));
		printk(KERN_INFO "loop %d\n", i);
		ret = __copy_to_user(ubuff, dqset->data[i], nbtr);	// shifting pointer to the next value
		kfree(dqset->data[i]);		// free the quantum that has been read
	  	dqset->data[i] = NULL;		// avoiding dangling pointer case
/*		#ifdef DEBUG
		printk(KERN_INFO "data : %s\n", (char *)ubuff);
		#endif
*/		ubuff = ubuff + ldev->reg_size;	// Even incase of failure, move the user space pointer by the size of a quantum	
		if(ret)						// > 0 for the no of bytes not written
		{
			#ifdef DEBUG
			printk(KERN_INFO "data bytes lost : %d\n", ret);
			#endif
		}		
		src += (nbtr - ret);				// successfully read characters	
		if ((i == (ldev->no_of_registers - 1)) || (dqset->data[i + 1] == NULL))
		{
			if (i == (ldev->no_of_registers - 1))		// reached last row of item
				ldev->first = ldev->first->next;	
			/* update ldev to point to next item only when last row encountered */
			kfree(dqset->data);			// free the entire data array that has been read
			dqset->data = NULL;			// avoiding dangling pointer case
			snext = dqset->next;			// preserve address of next item		
			kfree(dqset);				// free the item that has been read
			if (!snext)				// no next item
			{					// everything has been read	
				dqset = ldev->first = NULL;	// remove dangling case
				i = 0;				// reading shall be from beginning..for rd_flag	
				break;				// exit now
			}
			dqset = snext;			// updating to next item if value exists
			i = -1;
		}
		i++;
	}
	ldev->data_size = ldev->data_size - src;			// update datasize for this file structure
	ldev->rd_flag = i;
	#ifdef DEBUG
	printk(KERN_INFO "read and freed %d bytes from device\n", src);
	printk(KERN_INFO "new data size : %d\n", ldev->data_size);
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return src; 					// no of bytes successfully read
OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "Error\n");
	#endif
	return 0;
}
