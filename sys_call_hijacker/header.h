#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/slab.h>
#include<linux/cdev.h>
#include<linux/moduleparam.h>
#include<linux/uaccess.h>
#include<linux/wait.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("SANKALP NEGI");
MODULE_DESCRIPTION("Hijacking system calls");
MODULE_SUPPORTED_DEVICE("Test Device");
