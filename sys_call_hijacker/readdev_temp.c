#include"header.h"
#include"structures.h"
#include"declarations.h"

ssize_t readdev(struct file *file_ptr, char __user *ubuff, size_t size, loff_t *offset)
{
	Dev *ldev;
	Qset *dqset;
	int nbtr, flag, lsize, src, i, ret = 0, k;
	src = i = 0;
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	
	ldev = file_ptr->private_data;		// retrieving info from struct file
	printk(KERN_INFO "regsize : %d\n", ldev->reg_size);
	lsize = size;
	if (size > dev_size)
	{
		#ifdef DEBUG
		printk(KERN_INFO "Specified datasize is larger than device datasize\n");
		#endif
		lsize = dev_size;
	}						// lsize is the amount of data to read		
	dqset = ldev->first;	
	nbtr = reg_size;
	flag = 1;
	
	while(flag)
	{
		if (i == (no_of_registers - 1))		// we are last quantum of the item
		{
			if (!dqset->next)		// need to check if next item exists
				flag = 0;		// if true, exit after this iteration
		}
		else
		{
			if (!dqset->data[i])		// if this quantum is empty (NULL)
				break;			// exit now
		}
		nbtr = k = 0;	
		while (k < 4)			// check for each character in a quantum to be '\0'
		{
			if (*(char*)(dqset->data[i] + k) == '\0')	// if value of kth byte in ith row is '\0'
				break;
			nbtr = k;
			k++;
		}
		nbtr++;				// as nbtr is always 1 less when in loop, hence +1
		printk(KERN_INFO "loop %d\n", i);
		ret = __copy_to_user(ubuff, dqset->data[i], nbtr);	// shifting pointer to the next value
/*		#ifdef DEBUG
		printk(KERN_INFO "data : %s\n", (char *)ubuff);
		#endif
*/		ubuff = ubuff + reg_size;
		if(ret)						// > 0 for the no of bytes not written
		{
			#ifdef DEBUG
			printk(KERN_INFO "data bytes lost : %d\n", ret);
			#endif
		}			
		src += nbtr;				// successfully read characters	
		if (i == (no_of_registers - 1))
		{
			i = -1;
			if (!dqset->next)			// no next item
				break;
			dqset = dqset->next;	// ????	
		}
		i++;
	}
	ldev->data_size = src;				// update datasize for this file structure
	
	#ifdef DEBUG
	printk(KERN_INFO "data_size : %d\n", src);
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return src; 
/*OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "Error\n");
	#endif
	return 0;
*/
}
