#include"header.h"
#include"declarations.h"

int ioctl_ops(int fd)
{
	printf("\n%s : Begin\n", __func__);
	int choice;
	int arg;

	while (1)
	{
		printf("\n__________IOCTL Operations___________\n\n");
		printf("_____________DEVRESET    : 0___________\n");
		printf("_____________GETREGSIZE  : 1___________\n");
		printf("_____________SETREGSIZE  : 2___________\n");
		printf("_____________GETNOOFREG  : 3___________\n");
		printf("_____________SETNOOFREG  : 4___________\n");
		printf("_____________GETDEVSIZE  : 5___________\n");
		printf("_____________SETDEVSIZE  : 6___________\n");
		printf("_____________GETDATASIZE : 7___________\n");
		printf("_____________SETDATASIZE : 8___________\n");
		printf("_____________EXIT        : else___________\n\v");
		printf("Enter your choice -->>> : ");
		scanf("%d", &choice);
		switch (choice)
		{
			case 0:
				printf("######## DEVRESET selected ########  fd = %d\n", fd);
				if (ioctl(fd, DEVRESET) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Device was RESET\n");
				break;
			case 1:
				printf("######## GETREGSIZE ########  fd = %d\n", fd);
				if (ioctl(fd, GETREGSIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Got register size : %d\n", arg);
				break;
			case 2:
				printf("######## SETREGSIZE ########  fd = %d\n", fd);
				printf("Enter new reg_size to set : ");
				scanf("%d", &arg);
				if (ioctl(fd, SETREGSIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Register size was set to : %d\n", arg);
				break;
			case 3:
				printf("######## GETNOOFREG ########  fd = %d\n", fd);
				if (ioctl(fd, GETNOOFREG, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Got number of registers : %d\n", arg);
				break;
			case 4:
				printf("### SETNOOFREG ###  fd = %d\n", fd);
				printf("Enter new number of registers to set : ");
				scanf("%d", &arg);
				if (ioctl(fd, SETNOOFREG, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Number of registers was set to : %d\n", arg);
				break;
			case 5:
				printf("######## GETDEVSIZE ########  fd = %d\n", fd);
				if (ioctl(fd, GETDEVSIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Got device size : %d\n", arg);
				break;
			case 6:
				printf("### SETDEVSIZE ###  fd = %d\n", fd);
				printf("Enter new device size to set : ");
				scanf("%d", &arg);
				if (ioctl(fd, SETDEVSIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("device size was set to : %d\n", arg);
				break;
			case 7:
				printf("######## GETDATASIZE ########  fd = %d\n", fd);
				if (ioctl(fd, GETDATASIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Got data size : %d\n", arg);
				break;
			case 8:
				printf("### SETDATASIZE ###  fd = %d\n", fd);
				printf("Enter new data size to set : ");
				scanf("%d", &arg);
				if (ioctl(fd, SETDATASIZE, &arg) == -1)
				{	
					perror("ioctl failed :");
					break;
				}
				printf("Data size was set to : %d\n", arg);
				break;
			default:
				printf("Exiting !!\n");
				goto END;
		}
	}
END:	
	printf("\n%s : END\n", __func__);
	return 0;
}
