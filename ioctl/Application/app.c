#include"header.h"
#include"declarations.h"

int main()
{
	printf("\n%s : Begin \n", __func__);

	int choice, ch, fd = -1, ret;
	
	while(1)
	{
		choice = mainmenu();	
		switch (choice)
		{
			case 1 :
				fd = open("My_Dev", O_WRONLY);
				if (fd == -1)
				{
					perror("open() failed : ");
					break;
				}						
				printf("File Opened with fd = %d\n", fd);
				break;
			case 2 :
				if (fd == -1)
				{
					printf("%s : file not yet opened\n", __FILE__);
					break;
				}
				ret = ioctl_ops(fd);
				if (ret == -1)
				{
					perror("ioctl failed : ");
					break;
				}						
				break;
			case 5 :
				if (fd == -1)
				{
					printf("%s : file not yet opened\n", __FILE__);
					break;
				}
				close(fd);
				printf("File closed for fd = %d\n", fd);
				break;
			default:
				printf("\nOHK !! Exiting\n\v");
				goto END;
		}	
	}

END:	printf("\n%s : END\n\n", __func__);
	return 0;
}
