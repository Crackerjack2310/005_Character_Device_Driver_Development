int mainmenu();
int write_to_device(int);
int read_from_device(int);
int llseek_into_device(int);
int ioctl_ops(int);

// ######################################## IOCTL macros ##############################################

#ifndef MAGIC
#define MAGIC 'K'
#endif 

#ifndef DEVRESET
#define DEVRESET _IO(MAGIC, 0)
#endif

#ifndef GETREGSIZE
#define GETREGSIZE _IOR(MAGIC, 1,int)
#endif

#ifndef SETREGSIZE
#define SETREGSIZE _IOW(MAGIC, 2,int)
#endif 

#ifndef GETNOOFREG
#define GETNOOFREG _IOR(MAGIC, 3,int)
#endif

#ifndef SETNOOFREG
#define SETNOOFREG _IOW(MAGIC, 4,int)
#endif

#ifndef GETDEVSIZE
#define GETDEVSIZE _IOR(MAGIC, 5,int)
#endif

#ifndef SETDEVSIZE
#define SETDEVSIZE _IOW(MAGIC, 6,int)
#endif

#ifndef GETDATASIZE
#define GETDATASIZE _IOR(MAGIC, 7,int)
#endif

#ifndef SETDATASIZE
#define SETDATASIZE _IOW(MAGIC, 8,int)
#endif

