#include"header.h"
#include"structures.h"
#include"declarations.h"

long ioctldev(struct file *file_ptr, unsigned int cmd, unsigned long int arg)
{
	Dev *ldev = NULL;
	int retval = 0;
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	ldev = file_ptr->private_data;		// fetching the device
	if (!capable(CAP_SYS_ADMIN))			
	{
		printk("Not enough privileges..!!!\n");
		goto OUT;
	}
	#ifdef DEBUG
	printk(KERN_INFO "System has CAP_SYS_ADMIN privileges !!!\n");
	#endif

	if (!cmd)
		macro = 
	if (cmd%2 == 1)
		macro = _IOR 
	if (!cmd)
		macro = 
	switch (macro)
	{
		   case DEVRESET:
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : DEVRESET\n", __func__);
				#endif
				no_of_registers = ldev->no_of_registers = NO_OF_REG;
				reg_size = ldev->reg_size = REG_SIZE;
				dev_size = ldev->dev_size = DEV_SIZE;
				data_size = ldev->data_size = DATA_SIZE;
				#ifdef DEBUG
				printk(KERN_INFO "%s: ldev->no_of_registers : %d\n", __func__, ldev->no_of_registers);
				#endif
				#ifdef DEBUG
				printk(KERN_INFO "%s: ldev->reg_size        : %d\n", __func__, ldev->reg_size);
				#endif
				#ifdef DEBUG
				printk(KERN_INFO "%s: ldev->dev_size        : %d\n", __func__, ldev->dev_size);
				#endif
				#ifdef DEBUG
				printk(KERN_INFO "%s: ldev->data_size       : %d\n", __func__, ldev->data_size);
				#endif
				#ifdef DEBUG
				printk(KERN_INFO "%s : Device was RESET\n", __func__);
				#endif					
				break;
		case GETDEVSIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : GETDEVSIZE\n", __func__);
				#endif
/*				if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
*/				#ifdef DEBUG
				printk(KERN_INFO "%s: read access granted\n", __func__);
				#endif
				retval = put_user(dev_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __put_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				break;
		case SETDEVSIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : SETDEVSIZE\n", __func__);
				#endif
				if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: write access granted\n", __func__);
				#endif
				retval = __get_user(dev_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __get_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				ldev->dev_size = dev_size;
				break;
		case GETREGSIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : GETREGSIZE\n", __func__);
				#endif
				if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: read access granted\n", __func__);
				#endif
				retval = __put_user(reg_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __put_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				break;
		case SETREGSIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : SETREGSIZE\n", __func__);
				#endif
				if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: write access granted\n", __func__);
				#endif
				retval = __get_user(reg_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __get_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				ldev->reg_size = reg_size;
				break;
		case GETNOOFREG:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : GETNOOFREG\n", __func__);
				#endif
				if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: read access granted\n", __func__);
				#endif
				retval = __put_user(no_of_registers, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __put_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				break;
		case SETNOOFREG:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : SETNOOFREG\n", __func__);
				#endif
				if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: write access granted\n", __func__);
				#endif
				retval = __get_user(no_of_registers, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __get_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				ldev->no_of_registers = no_of_registers;
				break;
		case GETDATASIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : GETDATASIZE\n", __func__);
				#endif
				if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: read access granted\n", __func__);
				#endif
				retval = __put_user(data_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __put_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				break;
		case SETDATASIZE:	
				#ifdef DEBUG
				printk(KERN_INFO "%s: command : SETDATASIZE\n", __func__);
				#endif
				if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd)))
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: access_ok failed !!\n", __func__);
					#endif
					goto OUT;
				}
				#ifdef DEBUG
				printk(KERN_INFO "%s: write access granted\n", __func__);
				#endif
				retval = __get_user(data_size, (int __user *)arg);
				if (retval)		// error condition... 0 for success
				{
					#ifdef DEBUG
					printk(KERN_ERR "%s: __get_user failed with error code : %d\n", __func__, retval);
					#endif
					goto OUT;
				}
				ldev->data_size = data_size;
				break;
		default:
				#ifdef DEBUG
				printk(KERN_INFO "%s: Invalid choice\n", __func__);
				#endif
				goto OUT;
	}
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return 0; 
OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return -1;
}
