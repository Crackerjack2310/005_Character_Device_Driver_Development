#include"header.h"
#include"structures.h"
#include"declarations.h"
#include"fops.h"

unsigned int majorno, minorno, nod, dev_id, no_of_registers, reg_size, dev_size, data_size, ret;
dev_t devno, dev_id;
Dev *device;		/* pointer to user defined dev structure */

static int __init init_func(void)
{
	int dev_count;		/* for current device */
	nod = NOD;
	majorno = MAJORNO;
	minorno = MINORNO;
	no_of_registers = NO_OF_REG;
	reg_size = REG_SIZE;
	dev_size = DEV_SIZE;
	data_size = DATA_SIZE;
	
	device = (Dev*)kmalloc(sizeof(Dev)*nod, GFP_KERNEL);
	if(!device)
		goto OUT;	
	memset(device, '\0', (sizeof(Dev)*nod));		/* clear structure */

	ret = alloc_chrdev_region(&dev_id, minorno, nod, DEVNAME);
        if (ret == -1)
        	goto OUT;

        majorno = MAJOR(dev_id);
	
	#ifdef DEBUG
	printk(KERN_INFO "################## Hello Kernel !!! ###################\n");	
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "Major no : %d\n", majorno);
	#endif

	for(dev_count = 0; dev_count < nod; dev_count++)	/* for some 100 devices*/
	{
		device[dev_count].no_of_registers = no_of_registers;
		device[dev_count].reg_size = reg_size;
		device[dev_count].dev_size = dev_size;
		device[dev_count].data_size = data_size;		
		cdev_init(&device[dev_count].c_dev, &fops);	/* initialize the cdev structure before adding it */
		device[dev_count].c_dev.ops = &fops;		/*   */
		devno = MKDEV(majorno, dev_count);	
		ret = cdev_add(&device[dev_count].c_dev, devno, 1);
		if (ret == -1)
			goto OUT;
		#ifdef DEBUG
		printk(KERN_INFO "Minor no : %d\n", MINOR(device[dev_count].c_dev.dev));
		#endif
	}
	return 0;
OUT:	
	#ifdef DEBUG
	printk(KERN_ERR "alloc_chrdev_region failed with ret : %d\n", ret);
	#endif
	return 1;
}
module_init(init_func);
