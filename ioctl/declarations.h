int opendev(struct inode *, struct file *);
int releasedev(struct inode *, struct file *);
loff_t llseekdev(struct file *, loff_t, int);
ssize_t readdev(struct file *, char __user *, size_t, loff_t *);
ssize_t readdev_dest(struct file *, char __user *, size_t, loff_t *);
ssize_t writedev(struct file *, const char __user *, size_t, loff_t *);
int trim_device(Dev *);
Qset* create_dev_buff(size_t);
extern Dev *device;
extern unsigned int majorno, minorno, nod, dev_id, no_of_registers, reg_size, dev_size, data_size;
long ioctldev(struct file *, unsigned int, unsigned long int);

#ifndef DEVNAME
#define DEVNAME "Ioctl driver"
#endif

#ifndef MAJORNO 
#define MAJORNO 0
#endif

#ifndef MINORNO 
#define MINORNO 0
#endif

#ifndef NOD 
#define NOD 5
#endif

#ifndef DEBUG	
#define DEBUG
#endif

#ifndef NO_OF_REG	
#define NO_OF_REG 8
#endif

#ifndef REG_SIZE	
#define REG_SIZE 4
#endif

#ifndef DEV_SIZE	
#define DEV_SIZE 1024
#endif

#ifndef DATA_SIZE	
#define DATA_SIZE 0
#endif


//###################################### IOCTL Macros #################################################

#ifndef MAGIC
#define MAGIC 'K'
#endif

#ifndef DEVRESET
#define DEVRESET _IO(MAGIC, 0)
#endif

#ifndef GETREGSIZE
#define GETREGSIZE _IOR(MAGIC, 1,int)
#endif

#ifndef SETREGSIZE
#define SETREGSIZE _IOW(MAGIC, 2,int)
#endif

#ifndef GETNOOFREG
#define GETNOOFREG _IOR(MAGIC, 3,int)
#endif

#ifndef SETNOOFREG
#define SETNOOFREG _IOW(MAGIC, 4,int)
#endif

#ifndef GETDEVSIZE
#define GETDEVSIZE _IOR(MAGIC, 5,int)
#endif

#ifndef SETDEVSIZE
#define SETDEVSIZE _IOW(MAGIC, 6,int)
#endif


#ifndef GETDATASIZE
#define GETDATASIZE _IOR(MAGIC, 7,int)
#endif

#ifndef SETDATASIZE
#define SETDATASIZE _IOW(MAGIC, 8,int)
#endif
