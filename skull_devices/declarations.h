#ifndef DEVNAME
#define DEVNAME "Test Device"
#endif

#ifndef MAJORNO 
#define MAJORNO 0
#endif

#ifndef MINORNO 
#define MINORNO 0
#endif

#ifndef NOD 
#define NOD 5
#endif

#ifndef DEBUG	
#define DEBUG
#endif

#ifndef NO_OF_REG	
#define NO_OF_REG 10
#endif

#ifndef REG_SIZE	
#define REG_SIZE 8
#endif

#ifndef DEV_SIZE	
#define DEV_SIZE 1024
#endif

#ifndef DATA_SIZE	
#define DATA_SIZE 8
#endif
