#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/slab.h>
#include<linux/cdev.h>
#include<linux/moduleparam.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("SANKALP NEGI");
MODULE_DESCRIPTION("We provide major/minor numbers for all registered devices");
MODULE_SUPPORTED_DEVICE("Test Device");
