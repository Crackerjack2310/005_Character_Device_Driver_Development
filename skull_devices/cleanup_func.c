#include"header.h"
#include"structures.h"
#include"declarations.h"

static void __exit cleanup_func(void)
{
	int dev_count;
	nod = NOD;
	for(dev_count = 0; dev_count < nod; dev_count++)        /* for nod no of deviecs devices*/
              cdev_del(&device[dev_count].c_dev);
	
	printk(KERN_INFO "Bye kernel...See u again !!!\n");

	kfree(device);
	unregister_chrdev_region(dev_id, nod);
}
module_exit(cleanup_func);
