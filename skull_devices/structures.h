typedef struct Qset
{
	struct Qset *next;
	void **data;
}Qset;

typedef struct Dev
{
	struct Qset *first;
	struct cdev c_dev;
	int no_of_registers;
	int reg_size;
	int dev_size;
	int data_size;
}Dev;

extern Dev *device;
extern unsigned int majorno, minorno, nod, dev_id, no_of_registers, reg_size, dev_size, data_size;
