typedef struct Qset
{
	void **data;
	struct Qset *next;
}Qset;

typedef struct Dev
{
	struct Qset *first;
	spinlock_t slock;
	struct cdev c_dev;
	int no_of_registers;
	int reg_size;
	int dev_size;
	int data_size;
}Dev;

