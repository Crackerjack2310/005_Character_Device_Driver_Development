#include"header.h"
#include"structures.h"
#include"declarations.h"

loff_t llseekdev(struct file *file_ptr, loff_t offset, int origin)
{
	
	Dev *ldev = file_ptr->private_data;
	spin_lock(&ldev->slock);
	#ifdef DEBUG	
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif	
	#ifdef DEBUG									
	printk(KERN_INFO "%s : f_pos value : %ld\n", __func__,(long) file_ptr->f_pos);
	#endif
	switch(origin)
	{
		case 0:
			file_ptr->f_pos = offset;
			break;
		case 1:  
			file_ptr->f_pos += offset;
			break;
		case 2:
			file_ptr->f_pos = ldev->data_size - offset;
			break;
		default:
			#ifdef DEBUG	
			printk(KERN_INFO "%s : Invalid option for origin\n", __func__);
			#endif	
			spin_unlock(&ldev->slock);
			return -1;	
	}		
	#ifdef DEBUG									
	printk(KERN_INFO "%s : f_pos updated value : %ld\n", __func__,(long) file_ptr->f_pos);
	#endif		
	#ifdef DEBUG	
	printk(KERN_INFO "%s : End\n", __func__);
	#endif	
	spin_unlock(&ldev->slock);
	return file_ptr->f_pos;
}
