#include"header.h"
#include"declarations.h"
#include"structures.h"

unsigned int majorno, minorno, nod, ret;
dev_t dev_id;
static int __init init_func(void)
{
	Dev *device;
	device = (Dev*)kmalloc(sizeof(Dev), GFP_KERNEL);
	memset(device, '\0', sizeof(*device));
	printk(KERN_INFO "Hello Kernel !!!\n");

	majorno = MAJORNO;
	minorno = MINORNO;
	nod = NOD;
	
	ret = alloc_chrdev_region(&dev_id, minorno, nod, DEVNAME);
	if (ret == -1)
		goto OUT;

	printk(KERN_INFO "Major no : %d\n", MAJOR(dev_id));
	printk(KERN_INFO "Minor no : %d\n", MINOR(dev_id));
	return 0;
OUT:	
	printk(KERN_ERR "alloc_chrdev_region failed !!!\n");
	return 1;
}
module_init(init_func);
