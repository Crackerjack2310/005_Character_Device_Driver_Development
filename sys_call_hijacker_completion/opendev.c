#include"header.h"
#include"structures.h"
#include"declarations.h"

int opendev(struct inode *inode_ptr, struct file *file_ptr)
{
	Dev *ldev;	
	int ret;		

	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif

	ldev = container_of(inode_ptr->i_cdev, Dev, c_dev);
	if (!ldev)
	{
		#ifdef DEBUG
		printk(KERN_ERR "container_of failed\n");
		#endif
		goto OUT;
	}
	printk(KERN_INFO "regsize  : %d\n", ldev->reg_size);
	if (( file_ptr->f_flags & O_ACCMODE) == O_WRONLY )
	{
		ret = trim_device(ldev);
		if (ret == -1)
		{
			#ifdef DEBUG
			printk(KERN_ERR "Trim_device() failed\n");
			#endif
			goto OUT;
		}
	}
	file_ptr->private_data = ldev;			// for future use, storing the address of ldev
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return 0;
OUT:
	#ifdef DEBUG
	printk(KERN_ERR "%s : End with error. ret : %d\n", __func__, ret);
	#endif
	return -1;
}
