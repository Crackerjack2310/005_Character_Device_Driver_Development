typedef struct Qset
{
	void **data;
	struct Qset *next;
}Qset;

typedef struct Dev
{
	struct Qset *first;
	struct semaphore sem;
	struct cdev c_dev;
	int no_of_registers;
	int reg_size;
	int dev_size;
	int data_size;
	wait_queue_head_t myqueue;
}Dev;

