#include"header.h"
#include"structures.h"
#include"declarations.h"

loff_t llseekdev(struct file *file_ptr, loff_t offset, int origin)
{
	Dev *ldev = file_ptr->private_data;
 	#ifdef DEBUG	
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif	
	#ifdef DEBUG									
	printk(KERN_INFO "%s : f_pos value : %ld\n", __func__,(long) file_ptr->f_pos);
	#endif
	switch(origin)
	{
		case 0:
			file_ptr->f_pos = offset;
			break;
		case 1:  
			file_ptr->f_pos += offset;
			break;
		case 2:
			file_ptr->f_pos = ldev->data_size - offset;
			break;
		default:
			#ifdef DEBUG	
			printk(KERN_INFO "%s : Invalid option for origin\n", __func__);
			#endif	
			return -1;	
	}		
	#ifdef DEBUG									
	printk(KERN_INFO "%s : f_pos updated value : %ld\n", __func__,(long) file_ptr->f_pos);
	#endif		
	#ifdef DEBUG	
	printk(KERN_INFO "%s : End\n", __func__);
	#endif	
	return file_ptr->f_pos;
}
