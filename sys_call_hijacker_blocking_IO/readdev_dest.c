#include"header.h"
#include"structures.h"
#include"declarations.h"

ssize_t readdev_dest(struct file *file_ptr, char __user *ubuff, size_t size, loff_t *offset)
{
	Dev *ldev;
	Qset *dqset, *snext;
	int nbtr, lsize, src, i, ret = 0, k;
	src = i = 0;
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	ldev = file_ptr->private_data;		// retrieving info from struct file
	printk(KERN_INFO "%s : going to sleep...datasize = %d !!!\n", __func__, ldev->data_size);
	wait_event_interruptible(ldev->myqueue, ldev->data_size > 0);
	printk(KERN_INFO "%s : Woken up !!!\n", __func__);
	
	if (!ldev)
	{
		printk(KERN_INFO "Nothing to read !!!\n");
		goto OUT;
	}
	lsize = size;
	if (size > dev_size)
	{
		#ifdef DEBUG
		printk(KERN_INFO "Specified datasize is larger than device datasize\n");
		#endif
		lsize = dev_size;
	}						// lsize is the amount of data to read		
	snext = dqset = ldev->first;				
	if (!dqset)
	{
		printk(KERN_INFO "Nothing to read !!!\n");
		goto OUT;
	}
	nbtr = reg_size;
	while(src < lsize)
	{
		nbtr = k = 0;	
		while (k < 4)			// check for each character in a quantum to be '\0'
		{
			if (*(char*)(dqset->data[i] + k) == '\0')	// if value of kth byte in ith row is '\0'
				break;
			nbtr = k;
			k++;
		}
		nbtr++;				// as nbtr is always 1 less when in loop, hence +1
		nbtr = ((src + nbtr) < lsize) ? nbtr : (nbtr - ((src + nbtr) - lsize));
		printk(KERN_INFO "loop %d\n", i);
		ret = __copy_to_user(ubuff, dqset->data[i], nbtr);	// shifting pointer to the next value
		kfree(dqset->data[i]);		// free the quantum that has been read
		dqset->data[i] = NULL;		// avoiding dangling pointer case
/*		#ifdef DEBUG
		printk(KERN_INFO "data : %s\n", (char *)ubuff);
		#endif
*/		ubuff = ubuff + reg_size;
		if(ret)						// > 0 for the no of bytes not written
		{
			#ifdef DEBUG
			printk(KERN_INFO "data bytes lost : %d\n", ret);
			#endif
		}		
		src += (nbtr - ret);				// successfully read characters	
		if ((i == (no_of_registers - 1)) || (dqset->data[i + 1] == NULL))
		{
			kfree(dqset->data);			// free the entire data array that has been read
			dqset->data = NULL;			// avoiding dangling pointer case
			snext = dqset->next;			// preserve address of next item		
			kfree(dqset);				// free the item that has been read
			if (!snext)				// no next item
			{
				dqset = ldev->first = NULL;	// remove dangling case
				break;				// exit now
			}
			dqset = snext;			// updating to next item if value exists
			i = -1;
		}
		i++;
	}
	ldev->data_size = ldev->data_size - src;			// update datasize for this file structure
	
	#ifdef DEBUG
	printk(KERN_INFO "data_size : %d\n", ldev->data_size);
	#endif
	
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return src; 					// no of bytes successfully read
OUT:	
	#ifdef DEBUG
	printk(KERN_INFO "Error\n");
	#endif
	return -1;
}
